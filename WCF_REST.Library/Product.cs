﻿using System.Runtime.Serialization;

namespace WCF_REST.Library
{
	public class Product
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public double Price { get; set; }
	}
}
