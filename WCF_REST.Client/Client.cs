﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using WCF_REST.Library;

namespace WCF_REST.Client
{
	public class Client : ClientBase<IProductService>
	{
		public Client() { }

		public Client(string endpointConfigurationName): base(endpointConfigurationName) { }

		public Client(string endpointConfigurationName, string remoteAddress):base(endpointConfigurationName, remoteAddress) { }

		public Client(Binding binding, EndpointAddress remoteAddress):base(binding, remoteAddress) { }
	}
}
