﻿using System.Collections.Generic;
using WCF_REST.Library;

namespace WCF_REST.Service
{
	public class Products
	{
		private static List<Product> productsList = new List<Product>
		{
			new Product {Id = 1, Name = "Chleb", Description = "Chleb z makiem", Price = 2.40},
			new Product {Id = 2, Name = "Masło", Description = "Masło 250g", Price = 3.99},
			new Product {Id = 3, Name = "Bułka", Description = "Bułka kajzerka", Price = 0.50},
			new Product {Id = 4, Name = "Mleko", Description = "Mleko 3,2%", Price = 2.00},
			new Product {Id = 5, Name = "Woda", Description = "Woda niegazowana 1,5l", Price = 1.50}
		};

		public static List<Product> ProductsList
		{
			get
			{
				return productsList;
			}
		}
	}
}