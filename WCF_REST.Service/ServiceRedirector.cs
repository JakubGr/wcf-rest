﻿using System;
using System.Collections.Generic;
using System.Web;

namespace WCF_REST.Service
{
	public class ServiceRedirector : IHttpModule
	{
		private static List<Tuple<string, string>> services = new List<Tuple<string, string>>
		{
			new Tuple<string, string> ("productservice", "products")
		};

		public void Dispose() { }

		public void Init(HttpApplication app)
		{
			app.BeginRequest += delegate
			{
				HttpContext context = HttpContext.Current;
				string path = context.Request.AppRelativeCurrentExecutionFilePath.ToLower();
				Tuple<string, string> mapPath = services.Find(s => path.Contains(string.Format("/{0}/", s.Item2)) || path.EndsWith(string.Format("/{0}", s.Item2)));
				if (mapPath != null)
				{
					string newPath = path.Replace(string.Format("/{0}", mapPath.Item2), string.Format("/{0}.svc", mapPath.Item1));
					context.RewritePath(newPath, null, context.Request.QueryString.ToString(), false);
				}
			};
		}
		public static string RemoveSVCExtension(string path)
		{
			Tuple<string, string> mapPath = services.Find(s => path.Contains(string.Format("/{0}.svc/", s.Item1)) || path.EndsWith(string.Format("/{0}.svc", s.Item1)));
			if (mapPath != null)
			{
				path = path.Replace(string.Format("/{0}.svc", mapPath.Item1), string.Format("/{0}", mapPath.Item2));
			}
			return path;
		}
	}
}